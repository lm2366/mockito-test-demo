# unit-test-demo

#### 介绍
Mockito单元测试教程及样例
MockitoAPI文档:https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html
#### 单元测试依赖
<dependencies>
	<!--JUnit5的依赖包在这里就包括了-->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
	<!--JUnit4的依赖包-->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <!--TestNG依赖包-->
        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>7.4.0</version>
            <scope>test</scope>
        </dependency>
        <!--Mockito依赖包-->
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <version>4.5.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-inline</artifactId>
            <version>4.5.1</version>
            <scope>test</scope>
        </dependency>
</dependencies>

#### 功能介绍

1.  JUnitTest4AnnotationTest.java主要讲解JUnit4的常用注解
2.  JUnitTest5AnnotationTest.java主要讲解JUnit5的常用注解
3.  TestNgAnnotationTest.java主要讲解TestNG的常用注解
4.  MockitoAnnotationTest.java主要讲解Mockito的常用注解
5.  StaticAnnotationTest.java主要讲解如何使用Mockito模拟static方法
6.  MockConsAnnotationTest.java介绍了Mockito.mockConstruction方法的使用

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
