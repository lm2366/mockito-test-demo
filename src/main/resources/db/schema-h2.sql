DROP TABLE IF EXISTS t_user;

CREATE TABLE t_user
(
    id    BIGINT PRIMARY KEY NOT NULL COMMENT '主键ID',
    name  VARCHAR(30)        NULL DEFAULT NULL COMMENT '姓名',
    age   INT                NULL DEFAULT NULL COMMENT '年龄',
    email VARCHAR(50)        NULL DEFAULT NULL COMMENT '邮箱'
);

CREATE TABLE t_dept
(
    id        BIGINT PRIMARY KEY NOT NULL COMMENT '主键ID',
    parent_id BIGINT             NOT NULL COMMENT '父级部门ID',
    level     INT                NOT NULL COMMENT '部门层级',
    name      VARCHAR(30)        NULL DEFAULT NULL COMMENT '部门名称',
    age       INT                NULL DEFAULT NULL COMMENT '年龄',
    email     VARCHAR(50)        NULL DEFAULT NULL COMMENT '邮箱'
);

CREATE TABLE t_dept_user_relation
(
    id      BIGINT PRIMARY KEY NOT NULL COMMENT '主键ID',
    dept_id BIGINT             NOT NULL COMMENT '部门ID',
    user_id BIGINT             NOT NULL COMMENT '用户ID'
);