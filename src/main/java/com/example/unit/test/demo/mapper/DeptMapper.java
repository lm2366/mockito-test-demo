package com.example.unit.test.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.unit.test.demo.pojo.entity.DeptEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lijiadong
 * @date 2023/1/30 13:30
 */
@Mapper
public interface DeptMapper extends BaseMapper<DeptEntity> {
}
