package com.example.unit.test.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.unit.test.demo.exception.MyException;
import com.example.unit.test.demo.mapper.DeptMapper;
import com.example.unit.test.demo.pojo.entity.DeptEntity;
import com.example.unit.test.demo.pojo.entity.DeptUserRelationEntity;
import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.pojo.result.DeptUserResult;
import com.example.unit.test.demo.service.DeptService;
import com.example.unit.test.demo.service.DeptUserRelationService;
import com.example.unit.test.demo.service.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author lijiadong
 * @date 2023/1/30 13:30
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, DeptEntity> implements DeptService {

    @Autowired
    private UserService userService;

    @Autowired
    private DeptUserRelationService relationService;

    @Override
    public DeptUserResult getUserInfoById(Long userId) {
        DeptUserResult result = new DeptUserResult();
        try {
            UserEntity userEntity = userService.getById(userId);

            DeptUserRelationEntity relationEntity = relationService.getByUserId(userId);

            DeptEntity deptEntity = this.getById(relationEntity.getDeptId());

            result.setUserId(userEntity.getId());
            result.setUserName(userEntity.getName());
            result.setAge(userEntity.getAge());
            result.setUserEmail(userEntity.getEmail());
            result.setDeptId(deptEntity.getId());
            result.setParentId(deptEntity.getParentId());
            result.setDeptName(deptEntity.getName());
            result.setDeptPhoneNumber(deptEntity.getPhoneNumber());
            result.setDeptEmail(deptEntity.getEmail());

        } catch (Exception e) {
            throw new MyException("处理异常");
        }
        return result;
    }


    public void saveUserInfo(UserEntity userEntity, DeptUserRelationEntity relationEntity, DeptEntity deptEntity) {

        boolean userSave = userService.save(userEntity);

        boolean deptSave = this.save(deptEntity);

        relationService.add(relationEntity);


    }

    public UserEntity saveUserEntity(UserEntity userEntity) {
        Date date = new Date();
        // 被测试的静态方法
        Date tomorrow = DateUtils.addDays(date, 1);

        userEntity.setCreateTime(tomorrow);
        boolean save = userService.save(userEntity);
        return userEntity;
    }

}
