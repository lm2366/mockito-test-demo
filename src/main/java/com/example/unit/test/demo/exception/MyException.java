package com.example.unit.test.demo.exception;

/**
 * @author lijiadong
 * @date 2023/2/3 10:11
 */
public class MyException extends RuntimeException {

    public MyException(String message) {
        super(message);
    }
}
