package com.example.unit.test.demo.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author lijiadong
 * @date 2023/1/30 13:29
 */
@Data
@TableName("t_dept")
public class DeptUserRelationEntity {
    private Long id;
    private Long deptId;
    private Long userId;
}
