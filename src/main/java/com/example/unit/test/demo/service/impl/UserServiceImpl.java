package com.example.unit.test.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.unit.test.demo.mapper.UserMapper;
import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.service.UserService;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    @Override
    public void add(UserEntity entity) {
        this.save(entity);
    }

    @Override
    public void update(UserEntity entity) {
        this.updateById(entity);
    }

    @Override
    public UserEntity detail(String id) {
        UserEntity entity = this.getById(id);
        return entity;
    }

    @Override
    public List<UserEntity> list(UserEntity entity) {
        List<UserEntity> list = this.lambdaQuery()
                .eq(StringUtils.isNotEmpty(entity.getName()), UserEntity::getName, entity.getName())
                .eq(StringUtils.isNotEmpty(entity.getEmail()), UserEntity::getEmail, entity.getEmail())
                .list();

        return list;
    }

}
