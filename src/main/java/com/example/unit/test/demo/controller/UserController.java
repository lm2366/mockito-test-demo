package com.example.unit.test.demo.controller;

import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:34
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/save")
    public void save(@RequestBody UserEntity entity) {
        userService.add(entity);
    }

    @PutMapping("/update")
    public void update(UserEntity entity) {
        userService.update(entity);
    }

    @GetMapping("detail/{id}")
    public UserEntity detail(@PathVariable String id) {
        return userService.detail(id);
    }

    @GetMapping("list")
    public List<UserEntity> list(@RequestParam UserEntity entity) {
        return userService.list(entity);
    }
}
