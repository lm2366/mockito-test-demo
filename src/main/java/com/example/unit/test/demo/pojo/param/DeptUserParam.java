package com.example.unit.test.demo.pojo.param;

import lombok.Data;

/**
 * @author lijiadong
 * @date 2023/2/1 16:27
 */
@Data
public class DeptUserParam {

    private String userName;
    private Integer age;
    private String email;
    private Long deptId;

}
