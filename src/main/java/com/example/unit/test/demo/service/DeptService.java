package com.example.unit.test.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.unit.test.demo.pojo.entity.DeptEntity;
import com.example.unit.test.demo.pojo.result.DeptUserResult;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:31
 */
public interface DeptService extends IService<DeptEntity> {
    DeptUserResult getUserInfoById(Long userId);
}
