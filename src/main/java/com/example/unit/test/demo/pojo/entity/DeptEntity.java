package com.example.unit.test.demo.pojo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @author lijiadong
 * @date 2023/1/30 13:29
 */
@Data
@TableName("t_dept")
public class DeptEntity {
    private Long id;
    private Long parentId;
    private String name;
    private String phoneNumber;
    private String email;
    private Date createTime;
}
