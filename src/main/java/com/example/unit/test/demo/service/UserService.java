package com.example.unit.test.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.unit.test.demo.pojo.entity.UserEntity;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:31
 */
public interface UserService extends IService<UserEntity> {

    void add(UserEntity entity);

    void update(UserEntity entity);

    UserEntity detail(String id);

    List<UserEntity> list(UserEntity entity);
}
