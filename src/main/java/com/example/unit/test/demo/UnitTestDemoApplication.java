package com.example.unit.test.demo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.example.unit.test.demo.mapper")
public class UnitTestDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnitTestDemoApplication.class, args);
    }

}
