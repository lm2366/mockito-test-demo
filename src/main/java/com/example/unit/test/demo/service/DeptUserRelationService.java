package com.example.unit.test.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.unit.test.demo.pojo.entity.DeptUserRelationEntity;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:31
 */
public interface DeptUserRelationService extends IService<DeptUserRelationEntity> {

    void add(DeptUserRelationEntity entity);

    void update(DeptUserRelationEntity entity);

    DeptUserRelationEntity detail(String id);

    List<DeptUserRelationEntity> list(DeptUserRelationEntity entity);

    DeptUserRelationEntity getByUserId(Long userId);
}
