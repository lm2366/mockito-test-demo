package com.example.unit.test.demo.pojo.result;

import lombok.Data;

/**
 * @author lijiadong
 * @date 2023/2/1 16:32
 */
@Data
public class DeptUserResult {
    private Long userId;
    private String userName;
    private Integer age;
    private String userEmail;
    private Long deptId;
    private Long parentId;
    private String deptName;
    private String deptPhoneNumber;
    private String deptEmail;
}
