package com.example.unit.test.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.unit.test.demo.mapper.DeptUserRelationMapper;
import com.example.unit.test.demo.pojo.entity.DeptUserRelationEntity;
import com.example.unit.test.demo.service.DeptUserRelationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lijiadong
 * @date 2023/1/30 13:30
 */
@Service
public class DeptUserRelationServiceImpl extends ServiceImpl<DeptUserRelationMapper, DeptUserRelationEntity> implements DeptUserRelationService {

    @Override
    public void add(DeptUserRelationEntity entity) {
        this.save(entity);
    }

    @Override
    public void update(DeptUserRelationEntity entity) {
        this.updateById(entity);
    }

    @Override
    public DeptUserRelationEntity detail(String id) {
        DeptUserRelationEntity entity = this.getById(id);
        return entity;
    }

    @Override
    public List<DeptUserRelationEntity> list(DeptUserRelationEntity entity) {

        return null;
    }

    @Override
    public DeptUserRelationEntity getByUserId(Long userId) {
        DeptUserRelationEntity entity = this.lambdaQuery().eq(DeptUserRelationEntity::getUserId, userId).one();
        return entity;
    }

}
