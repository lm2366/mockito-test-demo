package com.example.unit.test.demo.service.impl;


import com.example.unit.test.demo.exception.MyException;
import com.example.unit.test.demo.mapper.DeptMapper;
import com.example.unit.test.demo.pojo.entity.DeptEntity;
import com.example.unit.test.demo.pojo.entity.DeptUserRelationEntity;
import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.pojo.result.DeptUserResult;
import com.example.unit.test.demo.service.DeptUserRelationService;
import com.example.unit.test.demo.service.UserService;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.testng.Assert;
import org.testng.annotations.*;

import java.lang.reflect.Method;

import static org.mockito.Mockito.*;

/**
 * @author lijiadong
 * @date 2023/1/31 10:02
 * <p>
 * 本类主要介绍Mockito相关注解的使用
 */
public class MockitoAnnotationTest {


    /**
     * <p>
     * @Mock、@Spy和@InjectMocks 的应用场景：
     * @Spy修饰的属性里面的方法可以真实执行,在需要的时候可以打桩模拟执行结果,使用方式是Mockito.doReturn().when()。
     * @Mock修饰的属性都是null,在执行单元测试的时候每个方法都需要打桩模拟执行结果,使用方式是Mockito.when().thenReturn()。
     * @InjectMocks通常注解在被测试类的对象上，使用@Mock和@Spy注解的属性都会被注入到被测试的对象里
     * 例如：
     * 我们要测试的目标类为DeptServiceImpl，类中有两个属性userService和relationService，使用@Mock注解的userService字段，
     * 以及使用@Spy注解的relationService字段，再执行测试方法的时候，就被会注入到service对象中，
     * 如果将service对象的注解改成@Spy，则userService和relationService都不会被注入到service对象中。
     * </p>
     */
    @InjectMocks
    private DeptServiceImpl service;
    @Mock
    private UserService userService;
    @Spy
    private DeptUserRelationService relationService;

    @Mock
    private DeptMapper baseMapper;

    private UserEntity userEntity;

    private DeptUserRelationEntity relationEntity;

    private DeptEntity deptEntity;

    private static final Long USER_ID = 1L;
    private static final Long DEPT_ID = 11L;
    private static final Long RELATION_ID = 1L;

    /**
     * @BeforeClass注解，所有测试方法执行之前，只执行一次
     */
    @BeforeClass
    public void beforeClass() {
        System.out.println("在所有测试方法前执行，只执行一次");

        MockitoAnnotations.openMocks(this);
    }

    /**
     * @AfterClass注解，所有测试方法执行之后，只执行一次，
     */
    @AfterClass
    public void afterClass() {
        System.out.println("在所有测试方法后执行，只执行一次");
    }

    /**
     * @BeforeMethod注解，每个测试方法执行之前，都会执行一次
     */
    @BeforeMethod
    public void before() {

        // 如果类注解没有使用@RunWith(MockitoJUnitRunner.class)，则必须添加下面的代码，否则不需要
        // MockitoAnnotations.openMocks(this);
        System.out.println("在每个测试方法前都会执行一次");
        userEntity = new UserEntity();
        userEntity.setId(USER_ID);
        userEntity.setName("Tom");
        userEntity.setAge(18);
        userEntity.setEmail("user@163.com");

        relationEntity = new DeptUserRelationEntity();
        relationEntity.setId(RELATION_ID);
        relationEntity.setDeptId(DEPT_ID);
        relationEntity.setUserId(USER_ID);

        deptEntity = new DeptEntity();
        deptEntity.setId(DEPT_ID);
        deptEntity.setName("研发部");
        deptEntity.setParentId(1L);
        deptEntity.setPhoneNumber("13800000000");
        deptEntity.setEmail("dept@163.com");
    }

    /**
     * @AfterMethod注解，每个测试方法执行之后，都会执行一次
     */
    @AfterMethod
    public void after() {
        System.out.println("在每个测试方法后都会执行一次");
    }

    /**
     * 主要介绍@Spy，@Mock
     * 以及Mockito.when...thenReturn，Mockito.doReturn...when...的用法
     */
    @Test(priority = 1)
    public void test_thenReturn_success() {
        // 使用@Mock注解的属性
        doReturn(userEntity).when(userService).getById(USER_ID);
        // when(userService.getById(USER_ID)).thenReturn(userEntity);

        // 使用@Spy注解的属性，如果不进行mock，会执行真实代码
        doReturn(relationEntity).when(relationService).getByUserId(USER_ID);
        // when(relationService.getByUserId(USER_ID)).thenReturn(relationEntity);

        // 由于mockito没有办法mock被测试对象的方法返回值，只能是这样来处理被测试对象的其他方法返回
        doReturn(deptEntity).when(baseMapper).selectById(DEPT_ID);

        // 执行被测试方法
        DeptUserResult result = service.getUserInfoById(USER_ID);

        // 返回结果进行断言
        Assert.assertEquals(result.getUserId(), USER_ID);
        Assert.assertEquals(result.getDeptId(), DEPT_ID);
    }

    /**
     * 主要介绍异常逻辑的单元测试方法
     */
    @Test(expectedExceptions = {MyException.class}, priority = 2)
    // @Test
    public void test_throwException_success() {
        // 使用@Mock注解的属性
        doReturn(userEntity).when(userService).getById(USER_ID);
        // when(userService.getById(USER_ID)).thenReturn(userEntity);

        // 使用@Spy注解的属性，如果不进行mock，会执行真实代码
        doReturn(relationEntity).when(relationService).getByUserId(USER_ID);

        // 可以使用doThrow的方法模拟异常
        doThrow(new MyException("错误")).when(baseMapper).selectById(DEPT_ID);
        // when(baseMapper.selectById(DEPT_ID)).thenThrow(new MyException("错误"));

        // 执行被测试方法
        DeptUserResult result = service.getUserInfoById(USER_ID);

        // 返回结果进行断言，可以使用@Test(expectedExceptions = {MyException.class})进行断言
        // 也可以使用以下进行断言
        // Assert.assertThrows(MyException.class, () -> service.getUserInfoById(USER_ID));

    }

    /**
     * 介绍如何使用doNothing以及如何对void返回值的方法进行断言
     */
    @Test(priority = 3)
    public void test_doNothing_success() {
        // 由于使用verify判断以下对象的方法执行次数，并且再其他方法中，也判断了这个类的执行次数，执行次数就会累加，导致次数判断不准确
        // 在这里可以使用此方法clear其他方法的调用次数
        clearInvocations(userService);
        clearInvocations(baseMapper);

        doReturn(true).when(userService).save(userEntity);

        // 对于void方法，可以使用doNothing进行处理
        doNothing().when(relationService).add(any());

        // 由于mockito没有办法mock被测试对象的方法返回值，只能是这样来处理被测试对象的其他方法返回
        // 当方法的参数有多个时，只要有一个参数使用了any函数，其他参数也必须是any的
        doReturn(1).when(baseMapper).insert(any(DeptEntity.class));

        // 执行被测试方法
        service.saveUserInfo(userEntity, relationEntity, deptEntity);

        // 由于saveUserInfo没有返回值，只能判断方法的执行次数
        // 可以指定某个对象的方法的执行次数time(1)
        // Mockito.any可以表示任意类型的数据，或者指定类型的数据
        verify(userService).save(userEntity);
        verify(userService, times(1)).save(any());
        verify(userService, times(1)).save(any(UserEntity.class));
        verify(baseMapper, times(1)).insert(any());

        // 除此之外，还有以下方法可供使用，在此不一一举例了
        // anyString();
        // anyBoolean();
        // anyByte();
        // anyShort();
        // anyInt();
        // anyLong();
        // anyChar();
        // anyCollection();
        // anyList();
        // anyMap();
        // anySet();
        // anyDouble();
        // anyFloat();
        // anyIterable();

    }

    @Test(priority = 4)
    public void test_doAnswer_success() {

        clearInvocations(userService);
        clearInvocations(baseMapper);

        // 可以使用doAnswer，对方法的执行过程进行模拟
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // 获取到mock对象，
                Object mock = invocationOnMock.getMock();
                System.out.println("mock:"+ mock.toString());
                // 获取到方法参数
                Object[] arguments = invocationOnMock.getArguments();
                if(arguments!=null) {
                    for(Object o : arguments) {
                        System.out.println("arg:"+ o);
                    }
                }
                // 获取到方法名称
                Method method = invocationOnMock.getMethod();
                System.out.println("method:"+ method.getName());
                return true;
            }
        }).when(userService).save(userEntity);

        // 由于mockito没有办法mock被测试对象的方法返回值，只能是这样来处理被测试对象的其他方法返回
        doReturn(1).when(baseMapper).insert(deptEntity);

        // 可以使用doAnswer，对方法的执行过程进行模拟
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                // 获取到mock对象，
                Object mock = invocationOnMock.getMock();
                System.out.println("mock:"+ mock.toString());
                // 获取到方法参数
                Object[] arguments = invocationOnMock.getArguments();
                if(arguments!=null) {
                    for(Object o : arguments) {
                        System.out.println("arg:"+ o);
                    }
                }
                // 获取到方法名称
                Method method = invocationOnMock.getMethod();
                System.out.println("method:"+ method.getName());

                return null;
            }
        }).when(relationService).add(relationEntity);

        // 执行被测试方法
        service.saveUserInfo(userEntity, relationEntity, deptEntity);

        // 由于saveUserInfo没有返回值，只能判断方法的执行次数
        // 可以指定某个对象的方法的执行次数time(1)
        verify(userService).save(userEntity);
        verify(userService, times(1)).save(any());
        verify(userService, times(1)).save(any(UserEntity.class));
        verify(baseMapper, times(1)).insert(any());

    }

}
