package com.example.unit.test.demo.controller;



import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.service.UserService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author lijiadong
 * @date 2023/1/31 10:02
 * <p>
 * 本类主要介绍TestNG注解的使用方法
 */
public class TestNgAnnotationTest {

    @InjectMocks
    private UserController controller;

    @Spy
    private UserService userService;

    private UserEntity entity;

    /**
     * @BeforeClass注解，所有测试方法执行之前，只执行一次，
     */
    @BeforeClass
    public void beforeClass() {
        System.out.println("在所有测试方法前执行，只执行一次");

        MockitoAnnotations.openMocks(this);
    }

    /**
     * @AfterClass注解，所有测试方法执行之后，只执行一次，
     */
    @AfterClass
    public void afterClass() {
        System.out.println("在所有测试方法后执行，只执行一次");
    }

    /**
     * @BeforeMethod注解，每个测试方法执行之前，都会执行一次
     */
    @BeforeMethod
    public void before() {
        // 如果类注解没有使用@RunWith(MockitoJUnitRunner.class)，则必须添加下面的代码，否则不需要
        // MockitoAnnotations.openMocks(this);
        System.out.println("在每个测试方法前都会执行一次");
        entity = new UserEntity();
        entity.setName("Tom");
        entity.setAge(18);
        entity.setEmail("unittest@163.com");
    }

    /**
     * @AfterMethod注解，每个测试方法执行之后，都会执行一次
     */
    @AfterMethod
    public void after() {
        System.out.println("在每个测试方法后都会执行一次");
    }

    @Test
    public void test_save_success() {
        doNothing().when(userService).add(entity);

        controller.save(entity);
        verify(userService).add(entity);
    }

    @Test
    public void test_detail_success() {
        doReturn(entity).when(userService).detail("id");
        UserEntity target = controller.detail("id");

        Assert.assertEquals(entity.getName(), target.getName());
        Assert.assertEquals(entity.getAge(), target.getAge());
        Assert.assertEquals(entity.getEmail(), target.getEmail());
    }

    @Test
    public void test_list_success() {
        List<UserEntity> list = Arrays.asList(entity);
        when(userService.list(entity)).thenReturn(list);

        List<UserEntity> target = controller.list(entity);
        Assert.assertEquals(entity.getName(), target.get(0).getName());
        Assert.assertEquals(entity.getAge(), target.get(0).getAge());
        Assert.assertEquals(entity.getEmail(), target.get(0).getEmail());
    }

}
