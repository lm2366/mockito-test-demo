package com.example.unit.test.demo.sender;

import com.aliyuncs.fc.client.FunctionComputeClient;
import com.aliyuncs.fc.response.InvokeFunctionResponse;
import com.example.unit.test.demo.pojo.entity.UserEntity;
import org.mockito.InjectMocks;
import org.mockito.MockedConstruction;
import org.mockito.MockitoAnnotations;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.HttpURLConnection;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author lijiadong
 * @date 2023/2/3 17:02
 */
public class MockConsAnnotationTest {
    @InjectMocks
    private FunctionSender fcSender;

    private FunctionComputeClient fcClient;

    private MockedConstruction<FunctionComputeClient> clientMockedConstruction;

    @BeforeClass
    public void beforeClass() {

        clientMockedConstruction = mockConstruction(FunctionComputeClient.class);
        MockitoAnnotations.openMocks(this);
        fcClient = clientMockedConstruction.constructed().get(0);

    }

    @AfterClass
    public void afterClass() {
        clientMockedConstruction.close();
    }

    @BeforeMethod
    public void setUp() {

    }

    @Test
    public void testSendMessage() {
        clearInvocations(fcClient);
        InvokeFunctionResponse invokeFunctionResponse = new InvokeFunctionResponse();
        invokeFunctionResponse.setStatus(HttpURLConnection.HTTP_ACCEPTED);
        invokeFunctionResponse.setHeader("X-Fc-Request-Id", "12345");
        doReturn(invokeFunctionResponse).when(fcClient).invokeFunction(any());
        this.fcSender.testSendUserInfo(new UserEntity(), "business_function_GZ");
        verify(fcClient).invokeFunction(any());
    }

    @Test
    public void testSenderInit() {
        FunctionSender sender = FunctionSender.getInstance();
        Assert.assertNotNull(sender);
    }

}
