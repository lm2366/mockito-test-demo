package com.example.unit.test.demo.service.impl;


import com.example.unit.test.demo.pojo.entity.UserEntity;
import com.example.unit.test.demo.service.UserService;
import org.apache.commons.lang3.time.DateUtils;
import org.mockito.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Date;

import static org.mockito.Mockito.*;

/**
 * @author lijiadong
 * @date 2023/1/31 10:02
 * <p>
 * 本类主要介绍Mockito相关注解的使用
 */
public class StaticAnnotationTest {

    @InjectMocks
    private DeptServiceImpl service;
    @Mock
    private UserService userService;

    /**
     * 静态类型的mock对象
     */
    private MockedStatic<DateUtils> dateUtilsMockedStatic;

    private UserEntity userEntity;

    private static final Long userId = 1L;

    @BeforeClass
    public void beforeClass() {
        System.out.println("在所有测试方法前执行，只执行一次");
        MockitoAnnotations.openMocks(this);

        // 初始化静态类型对象，由于静态类型对象再全局只能有一个，所以放在beforeClass中初始化
        dateUtilsMockedStatic = mockStatic(DateUtils.class);
    }

    @AfterClass
    public void afterClass() {
        System.out.println("在所有测试方法后执行，只执行一次");

        // 静态mock对象，再使用完成后，需要进行关闭，否则其他测试类无法再次初始化该静态类
        dateUtilsMockedStatic.close();

        Mockito.clearAllCaches();
    }

    /**
     * @BeforeMethod注解，每个测试方法执行之前，都会执行一次
     */
    @BeforeMethod
    public void before() {

        // 如果类注解没有使用@RunWith(MockitoJUnitRunner.class)，则必须添加下面的代码，否则不需要
        // MockitoAnnotations.openMocks(this);
        System.out.println("在每个测试方法前都会执行一次");
        userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setName("Tom");
        userEntity.setAge(18);
        userEntity.setEmail("user@163.com");
    }

    /**
     * @AfterMethod注解，每个测试方法执行之后，都会执行一次
     */
    @AfterMethod
    public void after() {
        System.out.println("在每个测试方法后都会执行一次");
    }

    /**
     * 介绍如何使用static
     */
    @Test
    public void test_static_success() {

        when(userService.save(userEntity)).thenReturn(true);

        // 在此处mock静态方法DateUtils方法的返回值
        // addDays中只要有一个参数使用any了，其他参数也必须是any的
        Date tomorrow = new Date();
        dateUtilsMockedStatic.when(() -> DateUtils.addDays(any(), anyInt())).thenReturn(tomorrow);
        // 执行被测试方法
        UserEntity userEntity = service.saveUserEntity(this.userEntity);

        Assert.assertEquals(userEntity.getCreateTime(), tomorrow);
    }


}
